﻿#Import sites from .csv
#Set-Location $PSScriptRoot
$csvFile = "c:\temp\SitesUrl.csv"
$table = Import-Csv $csvFile
$ErrorActionPreference= 'silentlycontinue'
Write-Host "Begin" -f Green


foreach ($row in $table)
{

 Write-Output $row.Url 
Connect-PnPOnline -Url $row.Url -ClientId 1664d1c4-58b2-41f8-9012-a79e81bf76b3 -ClientSecret 1f1VT3xYgjSy3-s97ZA~3z2.ysPH_R~8Kp 

#Get fields
try
{
$siteColumnMAKO = Get-PnPField -Identity "MAKO Id"
$siteColumnSC = Get-PnPField -Identity "Security classification"
$siteColumnDE = Get-PnPField -Identity "Description1"
$siteColumnCL = Get-PnPField -Identity "Classification"
$targetlist = Get-PnPList | Where-Object {$_.BaseTemplate -eq 101} 
}
        catch 
        {
Write-Error	"Fields Not Found On Site skipped"
Write-Host $_.ScriptStackTrace
break
}

$clientContext = Get-PnPContext
$targetlist | foreach {
$getlist = Get-PnPList | Where-Object {$_.Fields.Item -eq "MAKO Id"}
write-output = $_.Title

# Add field to list
    try
    {
$_.Fields.Add($siteColumnMAKO)
$_.Update()
$clientContext.ExecuteQuery()
Write-host "MAKOId OK" -f Green
}
        catch 
        {
Write-Warning	"MAKOId skipped"
}
    try
    {

$_.Fields.Add($siteColumnSC)
$_.Update()
$clientContext.ExecuteQuery()
Write-host "Security classification OK" -f Green
}
        catch 
        {
Write-Warning	"Security classification skipped"
}
    try
    {

$_.Fields.Add($siteColumnDE)
$_.Update()
$clientContext.ExecuteQuery()
Write-host "Description OK" -f Green
}
        catch 
        {
Write-Warning	"Description Skipped"
}
    try
    {

$_.Fields.Add($siteColumnCL)
$_.Update()
$clientContext.ExecuteQuery()
Write-host "Classification OK" -f Green
}
        catch 
        {
Write-Warning	"Classification Skipped"
}

#Update Fields View
       try
    {
$Updateview = Get-PnPList $_| Get-PnPView | Set-PnPView -Fields "Name", "Description", "Modified", "Modified By", "MAKO Id", "Sensitivity", "Classification", "Security classification", "ID", "Retention label"
Write-Host "  " 
Write-Host "    Field Display Updated           |          List update Completed" 
Write-Host "=========================================================================== " 

}
        catch 
        {
Write-Warning	"Field Display Skipped"
Write-Host $_.ScriptStackTrace
}
}
}